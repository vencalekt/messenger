﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Threading;
using System.Diagnostics;


namespace Messenger_APP
{
    class Sender :IDisposable
    {        
        private int port;
        private string hostIP;
        private TcpClient client;
        private Thread keepAliveThread;
        private Messenger messenger;


        public Sender(string hostIP, int port, Messenger messenger)
        {
            this.port = port;
            this.hostIP = hostIP;
            this.messenger = messenger;
        }


        public void StartSender()
        {
            Connect();
            keepAliveThread = new Thread(keepAlive);
            keepAliveThread.Start();
        }


        public void StopSender()
        {
            keepAliveThread = null;
            Disconnect();
        }


        //keeping the connection alive 
        private void keepAlive()
        {
            Thread.Sleep(1000);
            try
            {
                while (keepAliveThread != null)
                {
                    //conenction check
                    if (!connectionCheck())
                    {
                        Reconnect();
                    }
                    else
                    {
                        messenger.IsConnected = true;
                    }
                    Thread.Sleep(6000);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: {0}", e);
            }
        }


        //connect client to the server
        private void Connect( )
        {
            if (client == null)
            {
                try
                {
                    client = new TcpClient(hostIP, port);      
                }
                catch (SocketException e)
                {
                    Debug.WriteLine("SocketException: {0}", e);
                }
            }
        }


        //disconnect client from the server
        private void Disconnect()
        {
            if (client != null)
            {
                if(client.Connected)
                {
                    NetworkStream s = client.GetStream();
                    if (s != null)
                    {
                        s.Close();
                    }
                    //client.Close();
                }
                client.Close();
                client = null;
            }
            messenger.IsConnected = false;
        }


        private void Reconnect()
        {
            Disconnect();
            System.Threading.Thread.Sleep(1000);
            Connect();
        }


        private bool connectionCheck()
        {
            if (client == null)
            {
                return false;
            }
            else
            {
                if (!client.Connected)
                {
                    return false;
                }

                if(!client.Client.Connected)
                {
                    return false;
                }

                //
                if (!isClientConnected())
                {
                    client.Close();
                    return false;
                }
                return true;
            }
        }

        //send message
        public bool SendMessage(string message)
        {
            if (!connectionCheck())
            {
                Debug.WriteLine("Client is not connected!");
                return false;
            }
            try
            {
                Byte[] data = System.Text.Encoding.UTF8.GetBytes(message);

                NetworkStream stream = client.GetStream();
                stream.Write(data, 0, data.Length);

                Debug.WriteLine("Sent: {0}", message);
                data = new Byte[256];

                Int32 bytes = stream.Read(data, 0, data.Length);
                String responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                Debug.WriteLine("Received: {0}", responseData);
                stream.Flush();


                /*
                 * očekávám, že server odpoví stejný text, pro potvrzení správnosti
                 * 
                 */
                if(responseData.Equals(message))
                {
                    return true;
                }
                else 
                { 
                    return false; 
                }
            }
            catch (ArgumentNullException e)
            {
                Debug.WriteLine("ArgumentNullException: {0}", e);
                return false;
            }
            catch (SocketException e)
            {
                Debug.WriteLine("SocketException: {0}", e);
                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: {0}", e);
                client = null;
                return false;
            }
        }


        //testování, jestli je spojení skutečně živé
        private bool isClientConnected()
        {

            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnections = ipProperties.GetActiveTcpConnections()
                                                                    .Where(x => x.LocalEndPoint.Equals(client.Client.LocalEndPoint) 
                                                                             && x.RemoteEndPoint.Equals(client.Client.RemoteEndPoint))
                                                                    .ToArray();

            if (tcpConnections != null && tcpConnections.Length > 0)
            {
                TcpState stateOfConnection = tcpConnections.First().State;
                if (stateOfConnection == TcpState.Established)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public void Dispose()
        {
            StopSender();
            messenger = null;
        }
    }
}
