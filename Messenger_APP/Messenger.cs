﻿using System;
using System.Threading;


namespace Messenger_APP
{
    class Messenger
    {        
        private string partnerIp;
        private string localIp;
        private int partnerPort;
        private int localPort;   
        private bool isConnected;
        private FormMessenger mainWindow;


        private Reciever reciever;
        private Sender sender;  

        public bool IsConnected
        {
            get => isConnected;
            set
            {                
                if (value != isConnected)
                {
                    isConnected = value;
                    mainWindow.Invoke((Action)(() =>
                    {
                        mainWindow.UpdateFormControl(isConnected);
                    }));
                }                
            }
        }   
        

        public Messenger(FormMessenger opener)
        {
            partnerIp = Properties.Settings.Default.PartnerIP;
            partnerPort = Properties.Settings.Default.PartnerPort;
            localPort = Properties.Settings.Default.LoalPort;
            localIp = Properties.Settings.Default.LocalIp;

            mainWindow = opener;
            isConnected = false;

            reciever = new Reciever(localIp, localPort, this.RecieveMessage);
            sender = new Sender(partnerIp, partnerPort, this);
        }


        public void Start()
        {
            reciever.StartListener();            
            sender.StartSender();
        }


        public void Stop()
        {            
            reciever.StopListener();
            sender.StopSender();
        }


        public void SendMessage(string text)
        {
            if(sender.SendMessage(text))
            {
                OutcomeMessage msg = new OutcomeMessage(text, DateTime.Now);
                mainWindow.ShowMessage(msg);                
            }
            else
            {
                mainWindow.ShowErroMsg("Nepodařilo se odeslat zprávu", "Něco je špatně");
            }
        }

        
        public void RecieveMessage(string text)
        {
            IncommeMessage msg = new IncommeMessage(text, DateTime.Now);
            mainWindow.Invoke((Action)(() =>
            {
                mainWindow.ShowMessage(msg);
            }));
        }
    }
}
