﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Messenger_APP
{
    public abstract class Message
    {
        protected string Text { get; set; }
        protected DateTime DeliveryTime { get; set; }
        public Color TextColor { get; protected set; }

        public Message(string text, DateTime deliveryTime)
        {
            Text = text;
            DeliveryTime = deliveryTime;
            TextColor = Color.Black;
        }

        public abstract string FormatMessage();
    }

    class IncommeMessage : Message
    {
        public IncommeMessage(string text, DateTime deliveryTime) : base(text, deliveryTime)
        {
            TextColor = Color.Green;
        }

        public override string FormatMessage()
        {
            return $"<<<\t{DeliveryTime.ToString("dd. MM. yyyy HH:mm:ss")}: \n{Text}";
        }
    }

    class OutcomeMessage : Message
    {
        public OutcomeMessage(string text, DateTime deliveryTime) : base(text, deliveryTime)
        {
            TextColor = Color.Red;
        }

        public override string FormatMessage()
        {
            return $">>>\t{DeliveryTime.ToString("dd. MM. yyyy HH:mm:ss")}: \n{Text}";
        }
    }


}
