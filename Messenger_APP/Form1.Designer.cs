﻿
namespace Messenger_APP
{
    partial class FormMessenger
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.bntSend = new System.Windows.Forms.Button();
            this.txtMsgContent = new System.Windows.Forms.TextBox();
            this.raidoStatus = new System.Windows.Forms.RadioButton();
            this.richTextMsgHistory = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLocal = new System.Windows.Forms.Label();
            this.lblRemote = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bntSend
            // 
            this.bntSend.Enabled = false;
            this.bntSend.Location = new System.Drawing.Point(534, 41);
            this.bntSend.Margin = new System.Windows.Forms.Padding(2);
            this.bntSend.Name = "bntSend";
            this.bntSend.Size = new System.Drawing.Size(56, 44);
            this.bntSend.TabIndex = 0;
            this.bntSend.Text = "Odeslat";
            this.bntSend.UseVisualStyleBackColor = true;
            this.bntSend.Click += new System.EventHandler(this.bntSend_Click);
            // 
            // txtMsgContent
            // 
            this.txtMsgContent.Enabled = false;
            this.txtMsgContent.Location = new System.Drawing.Point(11, 41);
            this.txtMsgContent.Margin = new System.Windows.Forms.Padding(2);
            this.txtMsgContent.Multiline = true;
            this.txtMsgContent.Name = "txtMsgContent";
            this.txtMsgContent.Size = new System.Drawing.Size(505, 44);
            this.txtMsgContent.TabIndex = 1;
            // 
            // raidoStatus
            // 
            this.raidoStatus.AutoSize = true;
            this.raidoStatus.BackColor = System.Drawing.Color.Red;
            this.raidoStatus.Enabled = false;
            this.raidoStatus.Location = new System.Drawing.Point(534, 11);
            this.raidoStatus.Margin = new System.Windows.Forms.Padding(2);
            this.raidoStatus.Name = "raidoStatus";
            this.raidoStatus.Size = new System.Drawing.Size(55, 17);
            this.raidoStatus.TabIndex = 2;
            this.raidoStatus.TabStop = true;
            this.raidoStatus.Text = "Offline";
            this.raidoStatus.UseVisualStyleBackColor = false;
            // 
            // richTextMsgHistory
            // 
            this.richTextMsgHistory.Location = new System.Drawing.Point(11, 178);
            this.richTextMsgHistory.Margin = new System.Windows.Forms.Padding(2);
            this.richTextMsgHistory.Name = "richTextMsgHistory";
            this.richTextMsgHistory.ReadOnly = true;
            this.richTextMsgHistory.Size = new System.Drawing.Size(572, 391);
            this.richTextMsgHistory.TabIndex = 3;
            this.richTextMsgHistory.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Local:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(300, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Remote:";
            // 
            // lblLocal
            // 
            this.lblLocal.AutoSize = true;
            this.lblLocal.Location = new System.Drawing.Point(55, 11);
            this.lblLocal.Name = "lblLocal";
            this.lblLocal.Size = new System.Drawing.Size(35, 13);
            this.lblLocal.TabIndex = 6;
            this.lblLocal.Text = "label3";
            // 
            // lblRemote
            // 
            this.lblRemote.AutoSize = true;
            this.lblRemote.Location = new System.Drawing.Point(353, 11);
            this.lblRemote.Name = "lblRemote";
            this.lblRemote.Size = new System.Drawing.Size(35, 13);
            this.lblRemote.TabIndex = 7;
            this.lblRemote.Text = "label4";
            // 
            // FormMessenger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 580);
            this.Controls.Add(this.lblRemote);
            this.Controls.Add(this.lblLocal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextMsgHistory);
            this.Controls.Add(this.raidoStatus);
            this.Controls.Add(this.txtMsgContent);
            this.Controls.Add(this.bntSend);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormMessenger";
            this.Text = "Messenger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMessenger_FormClosing);
            this.Load += new System.EventHandler(this.FormMessenger_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bntSend;
        private System.Windows.Forms.TextBox txtMsgContent;
        private System.Windows.Forms.RadioButton raidoStatus;
        private System.Windows.Forms.RichTextBox richTextMsgHistory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLocal;
        private System.Windows.Forms.Label lblRemote;
    }
}

