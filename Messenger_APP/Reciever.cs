﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;


namespace Messenger_APP
{
    class Reciever
    {
        private int port;
        private IPAddress localAddr;
        private TcpListener listener;
        private Thread listenerThread;
        private List<TcpClient> clients;
        private Action<string> messageRecieved;


        public Reciever(string localIp, int port, Action<string> messageRecieved) 
        {
            this.port = port;
            localAddr = IPAddress.Parse(localIp);
            clients = new List<TcpClient>();
            this.messageRecieved = messageRecieved;
        }

        public void StartListener()
        {
            listenerThread = new Thread(RunListener);
            listenerThread.Start();
        }

        public void StopListener()
        {
            listenerThread = null;
            
            if(listener.Pending())
            {
                Thread.Sleep(500);
            }
            
            foreach (TcpClient c in clients)
            {
                NetworkStream s = c.GetStream();
                if(s!= null)
                {
                    s.Close(50);
                }
                c.Close();
            }
            listener.Server.Close(100);
            listener.Stop();
        }

        public void RunListener()
        {
            Debug.WriteLine("Waiting for a connection... ");
            listener = new TcpListener(localAddr, port);
            listener.Start();            
            try
            {
                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Debug.WriteLine(string.Format("\n{0} - New connection from  {1}", DateTime.Now.ToString("dd. MM. yyyy HH:mm:ss"), client.Client.RemoteEndPoint));
                    clients.Add(client);

                    ThreadPool.QueueUserWorkItem(ProcessClient, client);
                    Thread.Sleep(10);
                }
            }
            catch (SocketException e)
            {
                Debug.WriteLine("SocketException:", e);
            }
            catch(Exception e)
            {
                Debug.WriteLine("Exception:", e);
            }
        }

        
        private void ProcessClient(object state)
        {
            TcpClient client = state as TcpClient;

            try
            {
                Byte[] bytes = new Byte[256];
                String data = null;
                data = null;
                int i;

                NetworkStream stream = client.GetStream();                

                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                    Debug.WriteLine("Received: {0}", data);

                    messageRecieved.Invoke(data);

                    
                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                    /*
                    *jako odpověď, ověření odeslání odešleme, co přišlo
                    *bylo by ale lepší posílat jako odpověď součet bajtů, nebo hash
                    *aby se zbytečně neposílali dlouhé zprávy
                    */
                    stream.Write(msg, 0, msg.Length);
                    Debug.WriteLine("Sent: {0}", data);
                }
            }
            catch (SocketException e)
            {
                Debug.WriteLine("SocketException: {0}", e);
            }
            catch (Exception e)
            {
                Debug.WriteLine("SocketException: {0}", e);
            }
        }
    }
}
