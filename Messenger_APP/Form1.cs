﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace Messenger_APP
{
    public partial class FormMessenger : Form
    {
        private Messenger messenger;

        public FormMessenger()
        {
            InitializeComponent();            
        }

        //nacteni formulare
        private void FormMessenger_Load(object sender, EventArgs e)
        {
            messenger = new Messenger(this);

            //zobrazení nastavení local, remote
            lblLocal.Text = string.Format("{0}:{1}", Properties.Settings.Default.LocalIp, Properties.Settings.Default.LoalPort);
            lblRemote.Text = string.Format("{0}:{1}", Properties.Settings.Default.PartnerIP, Properties.Settings.Default.PartnerPort);

            messenger.Start();
        }

        //odeslani zpravy
        private void bntSend_Click(object sender, EventArgs e)
        {
            string text = txtMsgContent.Text;

            if (!string.IsNullOrEmpty(text))
            {
                messenger.SendMessage(text);
            }
        }

        //zobrazeni (prijate) zpravy
        public void ShowMessage(Message msg)
        {
            switch(msg.GetType().Name)
            {
                case "IncommeMessage":
                    AppendText(msg.FormatMessage(), msg.TextColor, true);
                        break;

                case "OutcomeMessage":
                    AppendText(msg.FormatMessage(), msg.TextColor, true);
                    txtMsgContent.Text = string.Empty;
                    break;
            }
        }

        //zobrazeni chbove hlasky
        public void ShowErroMsg(string message, string caption = "Something is wrong...")
        {
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //nastaveni ovladacich prvku formulare
        public void UpdateFormControl(bool isConnection)
        {
            if(isConnection)
            {
                raidoStatus.Checked = true;
                raidoStatus.BackColor = Color.Green;
                raidoStatus.Text = "Online";
                txtMsgContent.Enabled = true;
                bntSend.Enabled = true;
            }
            else
            {
                raidoStatus.Checked = false;
                raidoStatus.BackColor = Color.Red;
                raidoStatus.Text = "Offline";
                txtMsgContent.Enabled = false;
                bntSend.Enabled = false;
            }
        }

        //vlozeni textu do richtextboxu
        private void AppendText(string text, Color color, bool breakLine = false)
        {
            if (breakLine)
            {
                text += Environment.NewLine;
            }

            richTextMsgHistory.SelectionStart = richTextMsgHistory.TextLength;
            richTextMsgHistory.SelectionLength = 0;

            richTextMsgHistory.SelectionColor = color;
            richTextMsgHistory.AppendText(text);
        }

        //zavreni formulare
        private void FormMessenger_FormClosing(object sender, FormClosingEventArgs e)
        {
            messenger.Stop();
        }
    }
}
