# Messenger

Program pro jednoduchou textovou komunikaci mezi dvěma PC

## Seznam požadavků

### Obecne pozadavky a predpoklady
- program musi byt napsany v C# na libovolne platforme .NET
- musi bezet na Windows 10
- program musi byt uplne stejny na obou pocitacich
- pripadnou konfiguraci prebira z prikazove radky nebo externiho souboru
- IP adresy obou pocitacu jsou znamy a pocitace jsou spolu schopne komunikovat pomoci IPv4.

### Musi mit graficke rozhrani
- obsahuje textove pole pro zadani zpravy k odeslani
- obsahuje tlacitko pro odeslani
- zobrazuje seznam nekolika poslednich odeslanych a prijatych zprav
- vizualne odlisuje prijatou a odeslanou zparvu (staci textovym prefixem, zarovnanim vlevo/vpravo,
barvou textu atp)
- obsahuje indikator zivost spojeni s druhym pocitacem (jakykoliv dvoustavovy graficky prvek –
napr. CheckBox)

### Funkce
- ihned po startu programu se pokusi spojit s programem na druhem pocitaci (vysledek zobrazi
pomoci indikatoru)
- pri stisku tlacitka k odeslani se odesle text z textoveho pole na druhy pocitac
- jestlize neni do stanovene doby (cca. 5s) potvrzeno doruceni zpravy, zobrazi chybove hlaseni o
chybe
- jestlize je potvrzeno doruceni zpravy, zobrazi ji v seznamu odeslanych a prijatych zprav
jako odeslanou a smaze obsah textoveho pole
- pri prijmu zpravy odesle potvrzeni o prijmu zpravy a zobrazi ji v seznamu odeslanych a
prijatych zprav jako prijatou
- v radu malych sekund (cca. 8s) musi zjistit vypnuti programu na druhem pocitaci nebo preruseni
komunikace s nim (a zobrazovat pomoci indikatoru)


### Priklady chovani:
1. Je spusten program na pocitaci A. Indikator indikuje prerusenou komunikaci.
2. Je spusten program na pocitaci B. Indikator indikuje prerusenou komunikaci.
3. Po urcite dobre oba programy indikuji navazanou komunikaci.
4. Na pocitaci A je zadan text „Ahoj“ do textoveho pole
5. Na pocitaci A je stisknuto tlacitko pro odeslani
6. Na pocitaci B dojde k prijmu zpravy, zobrazuje zpravu ve svem seznamu zprav
7. Na pocitaci A dojde k potvrzeni prijmu zpravy, zobrazuje zpravu ve svem seznamu zprav
8. Je vypnut program na pocitaci A
9. Na pocitaci B je zadan text „Cau“ do textoveho pole a stisknuto tlacitko pro odeslani
10. Jelikoz je pocitac A vypnuty, nedojde k potvrzeni prijeti zpravy, a na pocitaci B je zobrazeno
chybove hlaseni, ze se odeslani nezdarilo
11. Na pocitaci B je indikovano preruseni komunikace s pocitacem A
12. Je znovu spusten program na pocitaci A. Indikator indikuje prerusenou komunikaci.
13. Po urcite dobre oba programy indikuji navazanou komunikaci.
14. Na pocitaci B je opet stisknuto tlacitko pro odeslani zpravy a je uspesne odeslana dle 6 a 7 ale
zrcadlove obracene.

## Spuštění
- pro správnou funkčnost apllikace je nutné mít povolený firewall
- konfigurace klientů se provádí přes *App.config
    - PartnerIP
    - PartnerPort
    - LocalPort
    - LocalIp

